# TimerTest


Background Tasks:

1) To keep the Timer in background i am using Executing finite length tasks of background capabilities. 
2) There is no direct way to run the timer in background, as iOs only provides the background capability to the following categories tasks:

a) Apps that start a short task in the foreground can ask for time to finish that task when the app moves to the background.
b) Apps that initiate downloads in the foreground can hand off management of those downloads to the system, thereby allowing the app to be suspended or terminated while the download continues.
c) Apps that need to run in the background to support specific types of tasks can declare their support for one or more background execution modes.





Always try to avoid doing any background work unless doing so improves the overall user experience. An app might move to the background because the user launched a different app or because the user locked the device and is not using it right now. In both situations, the user is signaling that your app does not need to be doing any meaningful work right now. Continuing to run in such conditions will only drain the device’s battery and might lead the user to force quit your app altogether. So be mindful about the work you do in the background and avoid it when you can.

